//click on item to cross it
$("ul").on("click", "li", function() {
	$(this).toggleClass("crossed");   
});

//click on X to delete that item
$("ul").on("click", "span", function(event) {
	$(this).parent().fadeOut(500, function() {
		$(this).remove();
	});
	event.stopPropagation();
});

//add new todo on enter pressed
$("input[type='text']").keypress(function(event){
	if (event.which === 13) {
		var newToDo = $(this).val();
		$(this).val("");
		$("ul").append("<li><span><i class='fa fa-trash'></i></span> " + newToDo + "</li>");
	}
});

//open input on plus icon pressed
$(".fa-plus").on("click", function() {
	$("input[type='text'").fadeToggle();
});